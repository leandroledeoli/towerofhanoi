# Tech Test

Write a program which can be used to solve the Tower of Hanoi's problem using WebAPI
 
## Requirements
 
1. It should provide a RESTFul API.

- 2. It should solve the Tower of Hanoi Problem
	a. It Should accept as parameter the number of discs for the problem
	b. It should return an identifier to monitor the execution

- 3. It should be able to execute at least 3 simulations at the same time
	a. It could have more than 3 requests simultaneously

- 4. It should be able to monitor the executions
	a. It should create an animations showing the execution