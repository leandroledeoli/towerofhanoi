﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TorreHanoiCliente.Entidades;
using TorreHanoiCliente.Util;

namespace TorreHanoiCliente.ViewModel
{
    class TorreHanoiViewModel : ViewModelBase
    {
        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="discos"></param>
        public TorreHanoiViewModel(int discos)
        {
            _torre = new TorreHanoi(discos);
            Discos = discos;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="discos"></param>
        /// <param name="tokenRequisicao"></param>
        public TorreHanoiViewModel(int discos, string tokenRequisicao)
        {
            _torre = new TorreHanoi(discos);
            Discos = discos;
            TokenRequisicao = tokenRequisicao;
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Representa se as movimentações ainda estão em processamento
        /// </summary>
        bool _emProcessamento;
        public bool EmExecucao
        {
            get { return _emProcessamento; }
            set
            {
                if (_emProcessamento != value)
                {
                    _emProcessamento = value;
                    OnPropertyChanged("EmExecucao");
                }
            }
        }

        /// <summary>
        /// Propriedade que representa o número de discos
        /// </summary>
        int _discos;
        public int Discos
        {
            get { return _discos; }
            set
            {
                if (value < 1 || value > 16 || _discos == value)
                    return;
                _discos = value;
                _torre = new TorreHanoi(value);
                OnPropertyChanged("Discos");
                OnPropertyChanged("ContadorMovimentos");
                Movimentos = null;
                EmExecucao = false;
            }
        }

        /// <summary>
        /// Propriedade que representa a velocidade da solução das torres
        /// </summary>
        double _velocidade = 5.0;
        public double Velocidade
        {
            get { return _velocidade; }
            set
            {
                _velocidade = value;
                OnPropertyChanged("Velocidade");
            }
        }

        /// <summary>
        /// Representa o contador dos movimento
        /// </summary>
        private int _contadorMovimentos = 0;
        public int ContadorMovimentos
        {
            get
            {
                return _contadorMovimentos;
            }
            set
            {
                _contadorMovimentos = value;
                OnPropertyChanged("ContadorMovimentos");
            }
        }

        ObservableCollection<Transicao> _movimentos;
        TorreHanoi _torre;
        ICommand _resolverCommand;

        /// <summary>
        /// Representa a lista de movimentos
        /// </summary>
        public ObservableCollection<Transicao> Movimentos
        {
            get { return _movimentos; }
            set
            {
                if (_movimentos != value)
                {
                    _movimentos = value;
                    OnPropertyChanged("Movimentos");
                }
            }
        }

        /// <summary>
        /// Representa a Data hora da chamada
        /// </summary>
        private string _dataHoraChamada;
        public string DataHoraChamada
        {
            get { return _dataHoraChamada; }
            set
            {
                _dataHoraChamada = value;
                OnPropertyChanged("DataHoraChamada");
            }
        }

        /// <summary>
        /// Representa a Data hora da finalização
        /// </summary>
        private string _dataHoraFinalizacao;
        public string DataHoraFinalizacao
        {
            get { return _dataHoraFinalizacao; }
            set
            {
                _dataHoraFinalizacao = value;
                OnPropertyChanged("DataHoraFinalizacao");
            }
        }

        /// <summary>
        /// Representa o token de requisição por simulação
        /// </summary>
        public string TokenRequisicao { get; set; }

        /// <summary>
        /// Representa o log das movimentações
        /// </summary>
        private string _logExecucao;
        public string LogExecucao
        {
            get { return _logExecucao; }
            set
            {
                _logExecucao = value;
                OnPropertyChanged("LogExecucao");
            }
        }
        #endregion

        #region ICommand
        /// <summary>
        /// ICommand responsável por iniciar animações
        /// </summary>
        public ICommand ResolverCommand
        {
            get
            {
                return _resolverCommand ?? (_resolverCommand =
                    new TransmitirCommand(() =>
                    {
                        EmExecucao = true;
                    }, () => !EmExecucao));
            }
        }

        #endregion

        #region Métodos
        /// <summary>
        /// Método responsável por realizar as movimentações
        /// </summary>
        public void FazerMovimento(Transicao movimento)
        {
            if (movimento != null)
                LogExecucao += movimento.ToString();

            ContadorMovimentos += 1;
        }

        /// <summary>
        /// Método que define as propriedades
        /// </summary>
        internal void MovimentosConcluido()
        {
            EmExecucao = false;
        }
        #endregion
    }
}
