﻿using Microsoft.Expression.Interactivity.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using TesteHanoiCliente;
using TorreHanoiCliente.Controle;
using TorreHanoiCliente.Entidades;

namespace TorreHanoiCliente.ViewModel
{
    /// <summary>
    /// View Model do Control TorreHanoiUI
    /// </summary>
    public class TorreHanoiClienteViewModel : ViewModelBase
    {
        #region Construtor
        /// <summary>
        /// Contrutor Default
        /// </summary>
        public TorreHanoiClienteViewModel()
        {
            ListaRequisicoesUsuario = new ObservableCollection<UsuarioRequisicao>() { new UsuarioRequisicao() };
            ListaControleSimulacoes = new Dictionary<TorreHanoiControleSolucao, UsuarioRequisicao>();
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Responsável por exibir o botão de limpar resultads
        /// </summary>
        private Visibility _exibirLimparSolucao = Visibility.Collapsed;
        public Visibility ExibirLimparSolucao
        {
            get
            {
                return _exibirLimparSolucao;
            }
            set
            {
                _exibirLimparSolucao = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Representa o dicionário com as simulações
        /// </summary>
        private Dictionary<TorreHanoiControleSolucao, UsuarioRequisicao> _listaControleSimulacoes = new Dictionary<TorreHanoiControleSolucao, UsuarioRequisicao>();
        public Dictionary<TorreHanoiControleSolucao, UsuarioRequisicao> ListaControleSimulacoes
        {
            get { return _listaControleSimulacoes; }
            set
            {
                _listaControleSimulacoes = value;
                OnPropertyChanged();
                OnPropertyChanged(() => ListaRequisicoesResultado);
            }
        }
        /// <summary>
        /// Propriedade responsável pelo texto "Quantidade Simulações" do TextBlock QuantidadeSimulacao
        /// </summary>
        public string QuantidadeSimulacoesTexto
        {
            get { return "Quantidade Simulações: "; }
        }

        /// <summary>
        /// Propriedade responsável pelo texto "Quantidade Simulações" do TextBlock QuantidadeSimulacao
        /// </summary>
        public string VelocidadeTexto
        {
            get { return "Velocidade: "; }
        }


        /// <summary>
        /// Propriedade responsável pelo texto "Simular Requisições" do Botão SimularRequisicoes
        /// </summary>
        public string SimularRquisicoesTexto
        {
            get { return "Simular Requisições"; }
        }

        /// <summary>
        /// Propriedade responsável pelo texto "Simular Requisições" do Botão SimularRequisicoes
        /// </summary>
        public string LimparResultadosTexto
        {
            get { return "Limpar Resultados"; }
        }

        /// <summary>
        /// Propriedade responsável pelo text "Quantidade de Discos" do TextBlock QuantidadeDisco
        /// </summary>
        public string QuantidadeDiscosTexto
        {
            get { return "Quantidade de Discos: "; }
        }


        /// <summary>
        /// Propriedade responsável pelo texto "Digite a Quantidade de Requisições que Deseja Simular" 
        /// do GroupBox grbSimularRequisicaoUsuario
        /// </summary>
        public string GroupQuantidadeRequisicaoTexto
        {
            get { return "Digite a Quantidade de Requisições que Deseja Simular : "; }
        }

        /// <summary>
        /// Propriedade responsável por obter o Index do ComboBox CmbQtdSimulacao
        /// </summary>
        private int _quantidadeSimulacaoUsuario = 0;
        public int QuantidadeSimulacaoPorUsuario
        {
            get { return _quantidadeSimulacaoUsuario; }
            set
            {
                /* Index 0 - Quantidade 1
                 * Index 1 - Quantidade 2
                 * Index 2 - Quantidade 3*/
                _velocidade = 5.0;
                _quantidadeSimulacaoUsuario = value;
                if (dispatcherTimer != null)
                    dispatcherTimer.Stop();
                GerarQuantidadeRequisicoesUsuario(_quantidadeSimulacaoUsuario);

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Propriedade que representa uma Requisição de Usuario
        /// </summary>
        private ObservableCollection<UsuarioRequisicao> _listaRequisicoesUsuario;
        public ObservableCollection<UsuarioRequisicao> ListaRequisicoesUsuario
        {
            get { return _listaRequisicoesUsuario; }
            set
            {
                _listaRequisicoesUsuario = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Propriedade que reoresenta a lista de Requisições realizadas
        /// </summary>
        private ObservableCollection<TorreHanoiControleSolucao> _listaRequisicoesResultado = new ObservableCollection<TorreHanoiControleSolucao>();
        public ObservableCollection<TorreHanoiControleSolucao> ListaRequisicoesResultado
        {
            get
            {

                return _listaRequisicoesResultado;
            }
            set
            {
                _listaRequisicoesResultado = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Reponsável por controlar se o botão ficará disponível
        /// </summary>
        private bool _bloquearBotaoGerarRequisicao = true;
        public bool IsEnabledBotaoGerarRequisicao
        {
            get { return _bloquearBotaoGerarRequisicao; }
            set
            {
                _bloquearBotaoGerarRequisicao = value;
                OnPropertyChanged();
            }
        }

        private double _velocidade = 5.0;
        public double Velocidade
        {
            get { return _velocidade; }
            set
            {
                _velocidade = value;

                if (ListaControleSimulacoes != null)
                {
                    foreach (var item in ListaControleSimulacoes)
                    {
                        (item.Key.DataContext as TorreHanoiViewModel).Velocidade = _velocidade;
                    }
                }
                OnPropertyChanged();
            }
        }
        #endregion

        #region ICommand
        /// <summary>
        /// Propriedade que representa o Command do botão Gerar Requisições
        /// </summary>
        private ICommand _iGerarRequisicoesCommand;
        public ICommand IGerarRequisicoesCommand
        {
            get
            {
                return _iGerarRequisicoesCommand ?? (_iGerarRequisicoesCommand = new ActionCommand(controle => GetSolucao(controle)));
            }
        }

        /// <summary>
        /// Propriedade que representa o Command do botão Incluir Novas Requisições
        /// </summary>
        private ICommand _iIncluirNovaRequisicao;
        public ICommand IIncluirNovaRequisicao
        {
            get
            {
                return _iIncluirNovaRequisicao ?? (_iIncluirNovaRequisicao = new ActionCommand(controle => GetNovaRequisicao(controle)));
            }
        }


        /// <summary>
        /// Propriedade que representa o Command do botão Gerar Requisições
        /// </summary>
        private ICommand _iLimparResultadosCommand;
        public ICommand ILimparResultadosCommand
        {
            get
            {
                return _iLimparResultadosCommand ?? (_iLimparResultadosCommand = new ActionCommand(controle => {
                    ListaControleSimulacoes = new Dictionary<TorreHanoiControleSolucao, UsuarioRequisicao>();
                    ListaRequisicoesResultado = new ObservableCollection<TorreHanoiControleSolucao>();
                    ListaRequisicoesUsuario = new ObservableCollection<UsuarioRequisicao>();
                    QuantidadeSimulacaoPorUsuario = 0;
                    IsEnabledBotaoGerarRequisicao = true;
                    ExibirLimparSolucao = Visibility.Collapsed;
                }));
            }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Método responsável por obter as informações solicitadas
        /// </summary>
        private async void GetSolucao(object controle)
        {
            GerarRequest(controle, ListaRequisicoesUsuario);
        }
        /// <summary>
        /// Método responsável por comunicar com a WebAPI
        /// </summary>
        /// <param name="controleRequest"></param>
        public async void GerarRequest(object controleRequest, ObservableCollection<UsuarioRequisicao> listaRequisicao)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60744");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                try
                {
                    foreach (var element in listaRequisicao)
                    {
                        int numeroDisco;
                        int.TryParse(element.Simulacao.NumeroDiscos, out numeroDisco);
                        if (numeroDisco > 0)
                        {
                            IsEnabledBotaoGerarRequisicao = false;
                            var getSolucao = string.Format("/api/TorreHanoi?codigoUsuario={0}&numeroDisco={1}", element.Id, element.Simulacao.NumeroDiscos);
                            HttpResponseMessage response = await client.GetAsync(getSolucao);
                            response.EnsureSuccessStatusCode();
                            var resultadoProcessamento = await response.Content.ReadAsStringAsync();
                            element.ResultadoProcessamento = JsonConvert.DeserializeObject<ResultadosTorreHanoi>(resultadoProcessamento);

                            IniciaAnimacaoSolucao(controleRequest, element);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }

        /// <summary>
        /// Responsável por criar novas requisições com base no parâmetros de disco
        /// </summary>
        /// <param name="controle"></param>
        public async void GetNovaRequisicao(object controle)
        {
            // Adicionar Novo Usuário
            var novaListaRequisicao = new ObservableCollection<UsuarioRequisicao>();
            foreach (var item in ListaRequisicoesUsuario)
            {
                novaListaRequisicao.Add(new UsuarioRequisicao { Simulacao = item.Simulacao });
            }

            GerarRequest(controle, novaListaRequisicao);
        }

        /// <summary>
        /// Métoro responsável por atualiar a lista de requisições / resultados
        /// </summary>
        /// <param name="torre"></param>
        /// <param name="usuario"></param>
        private void PopularColecoes(TorreHanoiControleSolucao torre, UsuarioRequisicao usuario)
        {
            ListaControleSimulacoes.Add(torre, usuario);

            if (ListaControleSimulacoes.Count != ListaRequisicoesResultado.Count)
            {
                foreach (var item in ListaControleSimulacoes)
                {
                    if (!ListaRequisicoesResultado.Contains(item.Key))
                    {
                        ListaRequisicoesResultado.Add(item.Key);
                    }
                    else
                    {
                        ListaRequisicoesResultado.ForEach(x =>
                        {
                            if ((x.DataContext as TorreHanoiViewModel).TokenRequisicao == (item.Key.DataContext as TorreHanoiViewModel).TokenRequisicao)
                            {
                                x = item.Key;
                            }
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Método responsável por iniciar a animação 
        /// </summary>
        DispatcherTimer dispatcherTimer;
        private void IniciaAnimacaoSolucao(object controleAtual, UsuarioRequisicao requisicao)
        {
            var controle = controleAtual as TorreHanoiUI;
            if (controle != null)
            {
                dispatcherTimer = new DispatcherTimer();
                int numeroDisco;
                int.TryParse(requisicao.Simulacao.NumeroDiscos, out numeroDisco);
                PopularColecoes(new TorreHanoiControleSolucao(numeroDisco, requisicao.ResultadoProcessamento.Transicao, requisicao.ResultadoProcessamento.TokenRequisicao), requisicao);

                dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                dispatcherTimer.Start();
            }
        }

        /// <summary>
        /// Método responsável por validar o processamento das simulações
        /// </summary>
        private async void CheckStatusProcessamento()
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:60744");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                foreach (var item in ListaControleSimulacoes)
                {
                    if (item.Value.ResultadoProcessamento.Status != ResultadosTorreHanoi.StatusResultado.Finalizado)
                    {
                        var ultimoMovimento = item.Value.ResultadoProcessamento.Transicao.Count;
                        var getStatus = string.Format("/api/TorreHanoi?tokenRequisicao={0}&ultimoMovimento={1}", item.Value.ResultadoProcessamento.TokenRequisicao, ultimoMovimento);

                        HttpResponseMessage response = await client.GetAsync(getStatus);
                        //response.EnsureSuccessStatusCode();
                        var resultadoProcessamento = await response.Content.ReadAsStringAsync();
                        item.Value.ResultadoProcessamento = JsonConvert.DeserializeObject<ResultadosTorreHanoi>(resultadoProcessamento);

                        //Console.WriteLine(string.Format("TOKEN: {0} TOTAL: {1}", item.Value.ResultadoProcessamento.TokenRequisicao, item.Value.ResultadoProcessamento.Transicao.Count));
                        AtualizaStatusAnimacao(item.Key, item.Value);
                        ValidaContinuar();
                    }
                }
            }
        }

        /// <summary>
        /// Método responsábel por validar se deve cotinuar com a validação de status
        /// </summary>
        private void ValidaContinuar()
        {
            if (ListaControleSimulacoes != null)
            {
                var existeItemParaAtualizar = false;
                foreach (var item in ListaControleSimulacoes)
                {
                    // Verificar se ainda existe alguma lista diferente de processado
                    if (item.Value.ResultadoProcessamento.Status != ResultadosTorreHanoi.StatusResultado.Finalizado)
                    {
                        existeItemParaAtualizar = true;
                        break;
                    }
                }
                if (!existeItemParaAtualizar)
                {
                    dispatcherTimer.Stop();
                    ExibirLimparSolucao = Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// Responsável pela atualização de transições
        /// </summary>
        /// <param name="controle"></param>
        /// <param name="requisicao"></param>
        public async void AtualizaStatusAnimacao(TorreHanoiControleSolucao controle, UsuarioRequisicao requisicao)
        {

            if (controle != null && requisicao != null)
            {
                if (requisicao.ResultadoProcessamento.Transicao.Count > 0 &&
                    requisicao.ResultadoProcessamento.Transicao.Count != (controle.DataContext as TorreHanoiViewModel).Movimentos.Count)
                {
                    foreach (var transicao in requisicao.ResultadoProcessamento.Transicao)
                    {
                        (controle.DataContext as TorreHanoiViewModel).Movimentos.Add(transicao);
                        (controle.DataContext as TorreHanoiViewModel).DataHoraChamada = requisicao.ResultadoProcessamento.DataHoraChamada;
                        (controle.DataContext as TorreHanoiViewModel).DataHoraFinalizacao = requisicao.ResultadoProcessamento.DataHoraFinalizacao;
                    }
                    controle.StartMovimentos();
                }
            }
        }

        /// <summary>
        /// Dispatcher que valida o status de processamento até que o status seja FINALIZADO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            CheckStatusProcessamento();
        }

        /// <summary>
        /// Método responsável por adicionar simulações máximo de [ 3 ]
        /// </summary>
        /// <param name="totalSimulacoesIndex"></param>
        private void GerarQuantidadeRequisicoesUsuario(int totalSimulacoesIndex)
        {
            ListaRequisicoesUsuario = new ObservableCollection<UsuarioRequisicao>();
            totalSimulacoesIndex += 1;

            for (int i = 0; i < totalSimulacoesIndex; i++)
            {
                ListaRequisicoesUsuario.Add(new UsuarioRequisicao());
            }
        }
        #endregion
    }
}
