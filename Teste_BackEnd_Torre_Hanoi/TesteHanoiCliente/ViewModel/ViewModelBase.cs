﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace TorreHanoiCliente.ViewModel
{
    /// <summary>
    /// Classe que representa como Base para os demais viewModels
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        #region Eventos
        /// <summary>
        /// Representa implementação do Property Changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Métodos Protegidos

        /// <summary>
        /// Método responsável por realizar a chamada do OnPropertyChanged
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="propertySelector"></param>
        protected void OnPropertyChanged<TValue>(Expression<Func<TValue>> propertySelector)
        {
            var memberExpression = propertySelector.Body as MemberExpression;
            if (memberExpression != null)
            {
                OnPropertyChanged(memberExpression.Member.Name);
            }
        }

        /// <summary>
        /// Método responsável por realizar a atualização da Propriedade modificada
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="propNames"></param>
        protected void OnPropertyChanged<TValue>(params Expression<Func<TValue>>[] propNames)
        {
            foreach (var name in propNames)
                OnPropertyChanged(name);
        }

        /// <summary>
        /// Método responsável por realizar a atualização da Propriedade através do nome
        /// </summary>
        /// <param name="name"></param>
        protected void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion

        #region Métodos Virtuais
        /// <summary>
        /// Método que definirá lógicas para atualização da VM
        /// </summary>
        public virtual void UpdateViewModel() { }

        /// <summary>
        /// Método responsável por limpar a propriedade
        /// </summary>
        public virtual void Dispose()
        {
            PropertyChanged = null;
        }
        #endregion
    }
}
