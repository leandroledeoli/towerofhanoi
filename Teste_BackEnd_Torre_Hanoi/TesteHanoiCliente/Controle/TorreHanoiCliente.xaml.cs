﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TorreHanoiCliente.ControlBase;
using TorreHanoiCliente.ViewModel;

namespace TorreHanoiCliente.Controle
{
    /// <summary>
    /// Interaction logic for TorreHanoiUI
    /// </summary>
    public partial class TorreHanoiUI : TorreHanoiClienteControlBase
    {
        #region Construtor

        /// <summary>
        /// Construtor Default
        /// </summary>
        public TorreHanoiUI()
        {
            InitializeComponent();

            // Define DataContext
            var vm = new TorreHanoiClienteViewModel();
            this.DataContext = vm;
        }
        #endregion
    }
}
