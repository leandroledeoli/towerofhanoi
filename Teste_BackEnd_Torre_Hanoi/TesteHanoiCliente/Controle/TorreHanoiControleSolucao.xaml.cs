﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TorreHanoiCliente.Entidades;
using TorreHanoiCliente.ViewModel;

namespace TorreHanoiCliente.Controle
{
    /// <summary>
    /// Interaction logic for TorreHanoiControleSolucao.xaml
    /// </summary>
    public partial class TorreHanoiControleSolucao : UserControl
    {
        #region Construtor
        /// <summary>
        /// Construtor padrão
        /// </summary>
        public TorreHanoiControleSolucao()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor que recebe o total de movimentos e token de requisição
        /// </summary>
        /// <param name="numeroDiscos"></param>
        /// <param name="movimentos"></param>
        /// <param name="tokenRequisicao"></param>
        public TorreHanoiControleSolucao(int numeroDiscos, ObservableCollection<Transicao> movimentos, string tokenRequisicao)
        {
            InitializeComponent();
            IniciaProcesso(numeroDiscos, movimentos, tokenRequisicao);
        }
        #endregion

        /// <summary>
        /// Reposnsábel por iniciar o processo das animações
        /// </summary>
        /// <param name="numeroDiscos"></param>
        /// <param name="movimentos"></param>
        /// <param name="tokenRequisicao"></param>
        private void IniciaProcesso(int numeroDiscos, ObservableCollection<Transicao> movimentos, string tokenRequisicao)
        {
            DataContext = _vm = new TorreHanoiViewModel(numeroDiscos, tokenRequisicao);
            _vm.Movimentos = movimentos;
            _vm.EmExecucao = true;
            _animacao = (Storyboard)FindResource("moveStoryboard");
            _vm.PropertyChanged += _vm_PropertyChanged;
            _vm.ResolverCommand.Execute(null);

            Init();
        }

        #region struct
        /// <summary>
        /// Struct que representa as torres
        /// </summary>
        struct PoleInfo
        {
            Stack<FrameworkElement> _discos;

            /// <summary>
            /// Método responsável por adicionar uma torre no controle
            /// </summary>
            /// <param name="element"></param>
            public void Add(FrameworkElement element)
            {
                if (_discos == null)
                    _discos = new Stack<FrameworkElement>();
                _discos.Push(element);
            }

            /// <summary>
            /// Propriedade que representa o Top da torre
            /// </summary>
            public FrameworkElement Top
            {
                get { return _discos.Peek(); }
            }

            /// <summary>
            /// Método reponsável por remover os discos da torre
            /// </summary>
            internal void Remove()
            {
                if (_discos != null && _discos.Any())
                    _discos.Pop();
            }

            /// <summary>
            /// representa o contador dos discos
            /// </summary>
            public int Count
            {
                get { return _discos == null ? 0 : _discos.Count; }
            }
        }
        #endregion

        TorreHanoiViewModel _vm;
        public Storyboard _animacao;
        const double TamanhoDisco = 60;

        IEnumerator<Transicao> _currentMoveEnum;
        PoleInfo[] _poles;

        #region Métodos
        /// <summary>
        /// Reponsável por atualizar as animações
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _vm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Discos":
                    Init();
                    break;

                case "Movimentos":
                    Init();
                    if (_vm.Movimentos != null)
                    {
                        _currentMoveEnum = _vm.Movimentos.GetEnumerator();
                        if (_currentMoveEnum.MoveNext())
                            MakeMove(_currentMoveEnum.Current);
                    }
                    break;
            }
        }

        /// <summary>
        /// Reponsável por iniciar os movimentos
        /// </summary>
        public async void StartMovimentos()
        {
            Init();
            if (_vm.Movimentos != null)
            {
                _currentMoveEnum = _vm.Movimentos.GetEnumerator();
                if (_currentMoveEnum.MoveNext())
                    MakeMove(_currentMoveEnum.Current);
            }
        }
        FrameworkElement _movendoDisco;

        /// <summary>
        /// Reponsábel por fazer o movimento dos discos
        /// </summary>
        /// <param name="movimento"></param>
        private void MakeMove(Transicao movimento)
        {
            int from = movimento.Origem, to = movimento.Destino;
            var disco = _movendoDisco = _poles[from].Top;
            var inicio = GetPositionInPole(disco, from);
            var fim = GetPositionInPole(disco, to);
            var estilo = FindResource("animGeometry") as PathGeometry;
            estilo.Figures[0].StartPoint = inicio;

            // update geometry
            var poly = estilo.Figures[0].Segments[0] as PolyLineSegment;
            poly.Points[0] = new Point(inicio.X, 800);
            poly.Points[1] = new Point(fim.X, 800);
            poly.Points[2] = fim;

            EventHandler completed = null;
            completed = (s, e) =>
            {
                _animacao.Completed -= completed;
                if (_vm.Movimentos != null)
                {
                    _poles[to].Add(disco);
                    _poles[from].Remove();
                    Canvas.SetLeft(disco, Canvas.GetLeft(disco));
                    Canvas.SetBottom(disco, Canvas.GetBottom(disco));
                    // next move
                    if (_currentMoveEnum != null)
                        if (_currentMoveEnum.MoveNext())
                        {
                            MakeMove(_currentMoveEnum.Current);
                        }
                        else
                        {
                            _vm.MovimentosConcluido();
                            CommandManager.InvalidateRequerySuggested();
                        }

                }
                _vm.FazerMovimento(_currentMoveEnum.Current);
            };
            _animacao.Completed += completed;
            // start a controllable animation
            _animacao.Begin(disco, true);
            
        }

        /// <summary>
        /// Reponsável por obter a posição da torre
        /// </summary>
        /// <param name="disc"></param>
        /// <param name="pole"></param>
        /// <returns></returns>
        private Point GetPositionInPole(FrameworkElement disc, int pole)
        {
            return new Point(200 + 400 * pole - disc.Width / 2, TamanhoDisco * _poles[pole].Count);
        }

        /// <summary>
        /// Responsável por iniciar as movimentações
        /// </summary>
        private void Init()
        {
            _currentMoveEnum = null;
            if (_movendoDisco != null)
                _animacao.Remove(_movendoDisco);
            _movendoDisco = null;
            _canvas.Children.RemoveRange(3, _canvas.Children.Count - 3);
            double width = 250, left = 200 - width / 2, bottom = 0;
            _poles = new PoleInfo[3];
            for (int i = 0; i < _vm.Discos; i++)
            {
                var disco = new Disco
                {
                    Texto = (_vm.Discos - i).ToString(),
                    FontSize = 30,
                    Width = width,
                    Height = TamanhoDisco,
                    Foreground = Brushes.White
                };
                _canvas.Children.Add(disco);
                Canvas.SetLeft(disco, left);
                Canvas.SetBottom(disco, bottom);
                width -= 10;
                bottom += TamanhoDisco;
                left += 5;
                _poles[0].Add(disco);
            }
        }
        #endregion
    }
}
