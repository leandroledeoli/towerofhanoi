﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TorreHanoiCliente.Controle
{
    /// <summary>
    /// Interaction logic for Disco.xaml
    /// </summary>
    public partial class Disco : UserControl
    {
        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        public Disco()
        {
            InitializeComponent();
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Representa o texto com o número de discos
        /// </summary>
        public string Texto
        {
            get { return (string)GetValue(TextoProperty); }
            set { SetValue(TextoProperty, value); }
        }

        /// <summary>
        /// Dependency Property responsável por atualizar o texto dos discos
        /// </summary>
        public static readonly DependencyProperty TextoProperty =
             DependencyProperty.Register("Texto", typeof(string), typeof(Disco), new UIPropertyMetadata(null));
        #endregion
    }
}
