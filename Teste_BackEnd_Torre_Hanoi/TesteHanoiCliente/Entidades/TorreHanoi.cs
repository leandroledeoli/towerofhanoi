﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TorreHanoiCliente.Entidades
{
    class TorreHanoi
    {
        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="discos"></param>
        public TorreHanoi(int discos)
        {
            Discos = discos;
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Representa o número de discos
        /// </summary>
        public int Discos { get; private set; }
        int _contadorMovimentos;

        /// <summary>
        /// Representa o contador de movimento
        /// </summary>
        public int ContadorMovimentos
        {
            get { return _contadorMovimentos; }
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Método responsável por realizar a movimentação dos discos
        /// </summary>
        /// <param name="discos"></param>
        /// <param name="inicio"></param>
        /// <param name="fim"></param>
        /// <param name="temp"></param>
        /// <returns></returns>
        private IEnumerable<Transicao> MoveDisco(int discos, int inicio, int fim, int temp)
        {
            if (discos == 1)
            {
                _contadorMovimentos++;
                yield return new Transicao(inicio, fim);
                yield break;
            }
            foreach (var move in MoveDisco(discos - 1, inicio, temp, fim))
            {
                yield return move;
            }
            foreach (var move in MoveDisco(1, inicio, fim, 0))
            {
                yield return move;
            }
            foreach (var move in MoveDisco(discos - 1, temp, fim, inicio))
            {
                yield return move;
            }
        }
        #endregion
    }
}
