﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TorreHanoiCliente.Entidades
{
    public class Transicao
    {
        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="destino"></param>
        /// <param name="origem"></param>
        public Transicao(int destino, int origem)
        {
            Destino = destino;
            Origem = origem;
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Representa a Origem do disco
        /// </summary>
        public int Origem { get; set; }

        /// <summary>
        /// Representa o destino do disco
        /// </summary>
        public int Destino { get; set; }

        public override string ToString()
        {
            return string.Format("Movimento de {0} para {1} \n", Origem, Destino);
        }
        #endregion
    }
}
