﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TorreHanoiCliente
{
    /// <summary>
    /// Classe que representa uma Simulação
    /// </summary>
    public class Simulacao
    {
        #region Propriedades
        /// <summary>
        /// Propriedade que representa o Número de Discos que será simulado
        /// </summary>
        public string NumeroDiscos { get; set; }
        #endregion
    }
}
