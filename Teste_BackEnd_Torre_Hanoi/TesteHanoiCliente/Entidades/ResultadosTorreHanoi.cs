﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TorreHanoiCliente.Entidades
{
    public class ResultadosTorreHanoi
    {
        #region Propriedades
        /// <summary>
        /// Representa o código do usuário
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Representa o token de requisição
        /// </summary>
        public string TokenRequisicao { get; set; }

        /// <summary>
        /// Representa a lista de transição com as movimentações
        /// </summary>
        public ObservableCollection<Transicao> Transicao { get; set; }

        /// <summary>
        /// Representa o número de discos
        /// </summary>
        public int NumeroDisco { get; set; }

        /// <summary>
        /// Representa o status de processamento
        /// </summary>
        public StatusResultado Status { get; set; }

        /// <summary>
        /// Representa a Data hora da chamada
        /// </summary>
        public string DataHoraChamada { get; set; }

        /// <summary>
        /// Representa a Data hora da finalização
        /// </summary>
        public string DataHoraFinalizacao { get; set; }

        /// <summary>
        /// Enum que representa o status de processamento
        /// </summary>
        public enum StatusResultado
        {
            Iniciado,
            Processando,
            Finalizado,
            Descartar,
        }
        #endregion
    }
}
