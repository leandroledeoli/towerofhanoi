﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TorreHanoiCliente.Entidades
{
    sealed class HanoiTransicao
    {
        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="origem"></param>
        /// <param name="destino"></param>
        public HanoiTransicao(int origem, int destino)
        {
            Origem = origem; Destino = destino;
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Representa a Origem dos discos
        /// </summary>
        public readonly int Origem;

        /// <summary>
        /// Repreesnta o Destino dos discos
        /// </summary>
        public readonly int Destino;
        #endregion

        public override string ToString()
        {
            return string.Format("Movimento de {0} para {1}", Origem, Destino);
        }
    }
}
