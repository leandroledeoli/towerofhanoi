﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TorreHanoiCliente.Entidades;

namespace TorreHanoiCliente
{
    /// <summary>
    /// Classe que representa a requisição realizada pelo Usuário
    /// </summary>
    public class UsuarioRequisicao
    {
        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        public UsuarioRequisicao()
        {
            Id = Guid.NewGuid().ToString();
            Simulacao = new Simulacao();
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Propriedade que representa o total de Requisicoes solicitadas
        /// </summary>
        public string QuantidadeTotalSimulacao { get; set; }

        /// <summary>
        /// Propriedade que representa a lista de Simulações que serão processadas
        /// </summary>
        public Simulacao Simulacao { get; set; }

        /// <summary>
        /// Propriedade que representa o Id do usuário
        /// </summary>
        public string Id { get; set; }

        public ResultadosTorreHanoi ResultadoProcessamento { get; set; }
        #endregion
    }
}
