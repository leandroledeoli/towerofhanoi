﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TorreHanoiCliente.Util
{
    public class TransmitirCommand<T> : ICommand
    {
        #region Construtor
        /// <summary>
        /// Construtor
        /// </summary>
        Action<T> _execute;
        Func<T, bool> _canExecute;
        public TransmitirCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute;
            _canExecute = canExecute;
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Responsável por executar um determinado ICommand
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public bool CanExecute(object parameter)
        {
            return _canExecute == null ? true : _canExecute((T)parameter);
        }

        /// <summary>
        /// Representa a atualização de um EventHandler
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (_canExecute != null)
                    CommandManager.RequerySuggested += value;
            }
            remove
            {
                if (_canExecute != null)
                    CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// Método responsável por executar o ICommand
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            _execute((T)parameter);
        }
        #endregion
    }

    public class TransmitirCommand : TransmitirCommand<object>
    {
        #region Construtor
        /// <summary>
        /// Responsável por transmitir o ICommand
        /// </summary>
        /// <param name="execute"></param>
        /// <param name="canExecute"></param>
        public TransmitirCommand(Action execute, Func<bool> canExecute = null)
            : base(_ => execute(), canExecute == null ? null :
        (Func<object, bool>)(_ => canExecute()))
        {
        }
        #endregion
    }
}
