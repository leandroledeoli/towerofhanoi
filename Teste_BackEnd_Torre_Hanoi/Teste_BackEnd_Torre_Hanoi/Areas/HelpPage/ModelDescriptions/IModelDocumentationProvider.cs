using System;
using System.Reflection;

namespace Teste_BackEnd_Torre_Hanoi.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}