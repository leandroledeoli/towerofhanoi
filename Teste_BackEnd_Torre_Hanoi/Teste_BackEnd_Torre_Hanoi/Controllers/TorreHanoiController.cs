﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Teste_BackEnd_Torre_Hanoi.Models;

namespace Teste_BackEnd_Torre_Hanoi.Controllers
{
    public class TorreHanoiController : ApiController
    {
        static readonly IProcessadorHanoi processador = new ProcessadorHanoi();

        #region Métodos Públicos
        /// <summary>
        /// responsável por requisitar a solução
        /// </summary>
        /// <param name="codigoUsuario"></param>
        /// <param name="numeroDisco"></param>
        /// <returns></returns>
        public ResultadosTorreHanoi GetHanoiResult(string codigoUsuario, int numeroDisco)
        {
            return processador.ProcessarAsync(codigoUsuario, numeroDisco);
        }

        /// <summary>
        /// Responsável por obter o status do andamento do processamento
        /// </summary>
        /// <param name="tokenRequisicao"></param>
        /// <param name="ultimoMovimento"></param>
        /// <returns></returns>
        public ResultadosTorreHanoi GetStatus(string tokenRequisicao, int ultimoMovimento)
        {
            if (ReservatorioHanoi.Processador.ContainsKey(tokenRequisicao))
                return new ResultadosTorreHanoi(ReservatorioHanoi.Processador[tokenRequisicao], ultimoMovimento);
            else
            {
                return null;
            }
        }
        #endregion
    }
}
