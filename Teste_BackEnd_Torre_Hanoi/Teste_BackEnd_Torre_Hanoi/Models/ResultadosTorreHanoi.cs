﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static Teste_BackEnd_Torre_Hanoi.Models.TorreHanoi;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    public class ResultadosTorreHanoi : IResultadosTorreHanoi<ResultadosTorreHanoi.StatusResultado>
    {
        #region Construtor

        /// <summary>
        /// Construtor Padrão
        /// </summary>
        public ResultadosTorreHanoi() { }

        /// <summary>
        /// Construtor que representa a criação de novos resultados
        /// </summary>
        /// <param name="codigoUsuario"></param>
        /// <param name="status"></param>
        /// <param name="tokenRequisicao"></param>
        /// <param name="numeroDisco"></param>
        public ResultadosTorreHanoi(string codigoUsuario, StatusResultado status, string tokenRequisicao, int numeroDisco)
        {
            CodigoUsuario = codigoUsuario;
            Status = status;
            TokenRequisicao = tokenRequisicao;
            Transicao = new List<Transicao>();
            NumeroDisco = numeroDisco;
            DataHoraChamada = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }

        /// <summary>
        /// Construtor que representa os resultados recuperados do dicionário
        /// </summary>
        /// <param name="simulacao"></param>
        /// <param name="ultimoLocalizado"></param>
        public ResultadosTorreHanoi(TorreHanoi simulacao, int ultimoLocalizado = 0)
        {
            CodigoUsuario = simulacao.CodigoUsuario;
            Status = simulacao.StatusTorre;
            TokenRequisicao = simulacao.TokenRequisicao;
            Transicao = simulacao.Transicao.GetRange(ultimoLocalizado, simulacao.Transicao.Count() - ultimoLocalizado);
            NumeroDisco = simulacao.NumeroDiscos;
            DataHoraChamada = simulacao.DataHoraChamada;
            DataHoraFinalizacao = simulacao.DataHoraFinalizacao;
        }
        #endregion

        #region Propriedades
        /// <summary>
        /// Representa o Código do Usuário
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Representa o staus de processamento
        /// </summary>
        public StatusResultado Status { get; set; }

        /// <summary>
        /// Representa a Data hora da chamada
        /// </summary>
        public string DataHoraChamada { get; set; }

        /// <summary>
        /// Representa a Data hora da finalização
        /// </summary>
        public string DataHoraFinalizacao { get; set; }

        /// <summary>
        /// Representa o token de requisição
        /// </summary>
        public string TokenRequisicao { get; set; }

        /// <summary>
        /// Representa o resultado com as transições
        /// </summary>
        public List<Transicao> Transicao { get; set; }

        /// <summary>
        /// Representa o número de Discos para o problema das torres
        /// </summary>
        public int NumeroDisco { get; set; }

        /// <summary>
        /// Enum que representa o status do processamento
        /// </summary>
        public enum StatusResultado
        {
            Iniciado,
            Processando,
            Finalizado,
            Descartar,
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Método responsável por iniciar a solução do problema
        /// </summary>
        /// <returns></returns>
        public TorreHanoi StartSolucao()
        {
            var simulacao = new TorreHanoi(CodigoUsuario, TokenRequisicao, NumeroDisco);
            Task.Run(() => { simulacao.ResolverProblema(); });
            return simulacao;
        }
        #endregion
    }
}