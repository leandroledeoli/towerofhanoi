﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    public class Transicao
    {
        #region Propriedades
        /// <summary>
        /// Representa a Origem da transição
        /// </summary>
        public int Origem { get; set; }

        /// <summary>
        /// Representa o destino da transição
        /// </summary>
        public int Destino { get; set; }
        #endregion
    }
}