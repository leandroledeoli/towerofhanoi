﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    public interface IProcessadorHanoi
    {
        #region Métodos
        /// <summary>
        /// Representa o Método responsável por obter o status do andamento do processamento
        /// </summary>
        /// <param name="tokenReq"></param>
        /// <param name="ultimoLocalizado"></param>
        /// <returns></returns>
        ResultadosTorreHanoi ObterStatus(string tokenReq, int ultimoLocalizado);

        /// <summary>
        /// Responsável por processar
        /// </summary>
        /// <param name="codigoUsuario"></param>
        /// <param name="numeroDisco"></param>
        /// <returns></returns>
        ResultadosTorreHanoi ProcessarAsync(string codigoUsuario, int numeroDisco);
        #endregion
    }
}
