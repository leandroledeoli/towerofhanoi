﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    public class TorreHanoi : ITorreHanoi
    {

        #region Construtor
        /// <summary>
        /// Construtor padrão
        /// </summary>
        public TorreHanoi()
        {

        }

        /// <summary>
        /// Construtor que iniciar os valores
        /// </summary>
        /// <param name="codigoUsuario"></param>
        /// <param name="tokenRequisicao"></param>
        /// <param name="numeroDisco"></param>
        public TorreHanoi(string codigoUsuario, string tokenRequisicao, int numeroDisco)
        {
            CodigoUsuario = codigoUsuario;
            TokenRequisicao = tokenRequisicao;
            NumeroDiscos = numeroDisco;
            DataHoraChamada = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            Transicao = new List<Transicao>();
            StatusTorre = ResultadosTorreHanoi.StatusResultado.Iniciado;
        }
        #endregion

        #region Constantes
        /// <summary>
        /// Representa o Inicio da Coluna
        /// </summary>
        public const int COLUNA_INICIO = 0;

        /// <summary>
        /// Representa a coluna temporária usada como intemédio para a solução
        /// </summary>
        public const int COLUNA_TEMP = 1;

        /// <summary>
        /// Representa o coluna final
        /// </summary>
        public const int COLUNA_FIM = 2;
        #endregion

        #region Propriedades

        /// <summary>
        /// Contador dos movimentos
        /// </summary>
        private int contador = 0;

        /// <summary>
        /// Representa o código do usuário
        /// </summary>
        public string CodigoUsuario { get; set; }

        /// <summary>
        /// Representa o token de requisição
        /// </summary>
        public string TokenRequisicao { get; set; }

        /// <summary>
        /// Representa a transição das movimentações
        /// </summary>
        public List<Transicao> Transicao { get; set; }

        /// <summary>
        /// Representa o número de discos
        /// </summary>
        public int NumeroDiscos { get; set; }

        /// <summary>
        /// Representa a Data hora da chamada
        /// </summary>
        public string DataHoraChamada { get; set; }

        /// <summary>
        /// Representa a Data hora da finalização
        /// </summary>
        public string DataHoraFinalizacao { get; set; }

        /// <summary>
        /// Representa o total de execuções
        /// </summary>
        public int TotalExecucoes { get; set; }

        /// <summary>
        /// Representa o status de processamento das torres
        /// </summary>
        public ResultadosTorreHanoi.StatusResultado StatusTorre { get; set; }
        #endregion

        #region Métodos
        /// <summary>
        /// Método responsável por resolver o problema
        /// </summary>
        /// <returns></returns>
        public TorreHanoi ResolverProblema()
        {
            StatusTorre = ResultadosTorreHanoi.StatusResultado.Processando;
            MoverTorre(NumeroDiscos, COLUNA_INICIO, COLUNA_FIM, COLUNA_TEMP);
            StatusTorre = ResultadosTorreHanoi.StatusResultado.Finalizado;
            return this;
        }

        /// <summary>
        /// Método responsável por realizar a movimentação
        /// </summary>
        /// <param name="qtdDisco"></param>
        /// <param name="inicio"></param>
        /// <param name="fim"></param>
        /// <param name="temp"></param>
        public void MoverTorre(int qtdDisco, int inicio, int fim, int temp)
        {
            if (qtdDisco == 1)
            {
                MoverDisco(inicio, fim);
            }
            else
            {
                MoverTorre(qtdDisco - 1, inicio, temp, fim);
                MoverDisco(inicio, fim);
                MoverTorre(qtdDisco - 1, temp, fim, inicio);

                DataHoraFinalizacao = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        /// <summary>
        /// Responsável por Mover os discos
        /// </summary>
        /// <param name="inicio"></param>
        /// <param name="fim"></param>
        public void MoverDisco(int inicio, int fim)
        {
            contador += 1;
            TotalExecucoes = contador;
            Transicao.Add(new Transicao() { Origem = inicio, Destino = fim });
        }
        #endregion
    }
}