﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    interface IResultadosTorreHanoi<T>
    {
        #region Propriedades
        /// <summary>
        /// Representa o código do usuário
        /// </summary>
        string CodigoUsuario { get; set; }

        /// <summary>
        /// Representa o status de processamento
        /// </summary>
        T Status { get; set; }

        /// <summary>
        /// Representa o token de requisição
        /// </summary>
        string TokenRequisicao { get; set; }

        /// <summary>
        /// Representa o resultado da solução com as transições
        /// </summary>
        List<Transicao> Transicao { get; set; }

        /// <summary>
        /// Representa o número de discos para a solução
        /// </summary>
        int NumeroDisco { get; set; }
        #endregion

        #region Métodos
        /// <summary>
        /// Representa o método que iniciara a solução do problema
        /// </summary>
        /// <returns></returns>
        TorreHanoi StartSolucao();
        #endregion
    }
}
