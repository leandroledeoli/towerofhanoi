﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    public class ProcessadorHanoi : IProcessadorHanoi
    {
        #region Métodos
        /// <summary>
        /// Método responsável por obter o status do processamento
        /// </summary>
        /// <param name="tokenReq"></param>
        /// <param name="ultimoLocalizado"></param>
        /// <returns></returns>
        public ResultadosTorreHanoi ObterStatus(string tokenReq, int ultimoLocalizado)
        {
            return new ResultadosTorreHanoi(ReservatorioHanoi.Processador[tokenReq], ultimoLocalizado);
        }

        /// <summary>
        /// Método responsável por realizar o processamento
        /// </summary>
        /// <param name="codigoUsuario"></param>
        /// <param name="numeroDisco"></param>
        /// <returns></returns>
        public ResultadosTorreHanoi ProcessarAsync(string codigoUsuario, int numeroDisco)
        {
            if (numeroDisco < 0)
                return null;

            var simulacao = new TorreHanoi(codigoUsuario, Guid.NewGuid().ToString(), numeroDisco);
            Task.Run(() => { simulacao.ResolverProblema(); });
            ReservatorioHanoi.Processador[simulacao.TokenRequisicao] = simulacao;

            return new ResultadosTorreHanoi(simulacao);
        }
        #endregion
    }
}