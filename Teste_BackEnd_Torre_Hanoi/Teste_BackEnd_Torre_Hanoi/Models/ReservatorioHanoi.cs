﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    public static class ReservatorioHanoi
    {
        #region Propriedades
        /// <summary>
        /// Representa o dicionário de resultados processados
        /// </summary>
        private static Dictionary<string, TorreHanoi> _processador = new Dictionary<string, TorreHanoi>();
        public static Dictionary<string, TorreHanoi> Processador
        {
            get { return _processador; }
            set { _processador = value; }
        }
        #endregion
    }
}
