﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teste_BackEnd_Torre_Hanoi.Models
{
    public interface ITorreHanoi
    {
        #region Propriedades
        /// <summary>
        /// Representa o código do usuário
        /// </summary>
        string CodigoUsuario { get; set; }

        /// <summary>
        /// Representa o token de requisição
        /// </summary>
        string TokenRequisicao { get; set; }

        /// <summary>
        /// Representa o resultado das transições
        /// </summary>
        List<Transicao> Transicao { get; set; }

        /// <summary>
        /// Representa o número de discos
        /// </summary>
        int NumeroDiscos { get; set; }

        /// <summary>
        /// Representa o total de execuções
        /// </summary>
        int TotalExecucoes { get; set; }

        /// <summary>
        /// Representa a Data hora da chamada
        /// </summary>
        string DataHoraChamada { get; set; }

        /// <summary>
        /// Representa a Data hora da finalização do processamento
        /// </summary>
        string DataHoraFinalizacao { get; set; }
        #endregion

        #region Métodos
        /// <summary>
        /// Método que resolve o problema das torres
        /// </summary>
        /// <returns></returns>
        TorreHanoi ResolverProblema();

        /// <summary>
        /// Método responsável por realizar as movimentações
        /// </summary>
        /// <param name="qtdDisco"></param>
        /// <param name="inicio"></param>
        /// <param name="fim"></param>
        /// <param name="temp"></param>
        void MoverTorre(int qtdDisco, int inicio, int fim, int temp);

        /// <summary>
        /// Método responsável por realizar as movimentações de discos
        /// </summary>
        /// <param name="inicio"></param>
        /// <param name="fim"></param>
        void MoverDisco(int inicio, int fim);
        #endregion
    }
}
