﻿using System.Web;
using System.Web.Mvc;

namespace Teste_BackEnd_Torre_Hanoi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
