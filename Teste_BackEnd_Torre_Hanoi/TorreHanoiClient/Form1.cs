﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TorreHanoiClient
{
    public partial class frmSimular : Form
    {
        public frmSimular()
        {
            InitializeComponent();
        }

        private void btnSimularRequisicoes_Click(object sender, EventArgs e)
        {
            CriarRequisicoes(txtQtdRequisicoes.Text);
        }

        private void CriarRequisicoes(string qtdRequisicoes)
        {
            int totalRequisicoes;
            int.TryParse(qtdRequisicoes, out totalRequisicoes);

            AjustaLayoutSetRequisicoesUsuario();
        }

        private void AjustaLayoutSetRequisicoesUsuario()
        {

        }
    }
}
