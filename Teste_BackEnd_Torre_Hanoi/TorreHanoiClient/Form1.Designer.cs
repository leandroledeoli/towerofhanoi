﻿namespace TorreHanoiClient
{
    partial class frmSimular
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtQtdRequisicoes = new System.Windows.Forms.TextBox();
            this.btnSimularRequisicoes = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSimularRequisicoes);
            this.groupBox1.Controls.Add(this.txtQtdRequisicoes);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(327, 109);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informe a Quantidade de Requisições que Deseja Simular:";
            // 
            // txtQtdRequisicoes
            // 
            this.txtQtdRequisicoes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtQtdRequisicoes.Location = new System.Drawing.Point(104, 33);
            this.txtQtdRequisicoes.Name = "txtQtdRequisicoes";
            this.txtQtdRequisicoes.Size = new System.Drawing.Size(116, 23);
            this.txtQtdRequisicoes.TabIndex = 0;
            // 
            // btnSimularRequisicoes
            // 
            this.btnSimularRequisicoes.Location = new System.Drawing.Point(104, 65);
            this.btnSimularRequisicoes.Name = "btnSimularRequisicoes";
            this.btnSimularRequisicoes.Size = new System.Drawing.Size(116, 38);
            this.btnSimularRequisicoes.TabIndex = 1;
            this.btnSimularRequisicoes.Text = "Simular";
            this.btnSimularRequisicoes.UseVisualStyleBackColor = true;
            this.btnSimularRequisicoes.Click += new System.EventHandler(this.btnSimularRequisicoes_Click);
            // 
            // frmSimular
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 134);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmSimular";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSimularRequisicoes;
        private System.Windows.Forms.TextBox txtQtdRequisicoes;
    }
}

