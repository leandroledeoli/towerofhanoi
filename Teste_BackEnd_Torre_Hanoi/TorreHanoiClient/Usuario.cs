﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TorreHanoiClient
{
    public class Usuario
    {
        public int QuantidadeTotalSimulacao { get; set; }
        public List<Simulacao> Simulacao { get; set; }
    }
}
